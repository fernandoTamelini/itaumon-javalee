package br.javalee.itaumon.dao;

import org.springframework.data.repository.CrudRepository;

import br.javalee.itaumon.model.Usuario;

public interface UsuarioDAO extends CrudRepository<Usuario, Integer>{

    public Usuario findByRacfOrFuncional(String racf, int funcional);
    public Usuario findBySenha(String senha);
    
}


