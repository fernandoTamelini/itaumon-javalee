package br.javalee.itaumon.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.javalee.itaumon.model.Feriado;

public interface FeriadoDAO extends CrudRepository<Feriado, Integer>{
    public List<Feriado> findByAgencia(Integer agencia);

    @Query(value = "Select new Feriado(f.idFeriado, f.dataInicio, f.dataFim, f.nomeFeriado) From Feriado f join Agencia a on a.idAgencia = f.agencia Where a.numAgencia = :id or a.numAgencia =1")
    public List<Feriado> buscarPorAgencia(@Param("id") Integer Id);

    

}
