package br.javalee.itaumon.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.javalee.itaumon.model.Agencia;

public interface AgenciaDAO extends CrudRepository<Agencia, Integer>{
    public List<Agencia> findByNumAgenciaGreaterThan(Integer agencia);
}
