package br.javalee.itaumon.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.javalee.itaumon.dao.AgenciaDAO;
import br.javalee.itaumon.model.Agencia;




@RestController
@CrossOrigin("*")
public class AgenciaController {
    @Autowired
    private AgenciaDAO dao;

    @GetMapping("/agencia/todas")
    public ResponseEntity<List<Agencia>> buscarTodas(){
        List<Agencia> lista = (List<Agencia>)dao.findByNumAgenciaGreaterThan(1);
 
        if(lista != null){
            return ResponseEntity.ok(lista);
        }else{
            return ResponseEntity.status(404).build();
        }

    }    
    


    
}
