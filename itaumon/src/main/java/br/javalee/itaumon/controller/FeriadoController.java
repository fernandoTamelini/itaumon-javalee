package br.javalee.itaumon.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.javalee.itaumon.dao.FeriadoDAO;

import br.javalee.itaumon.model.Feriado;

@RestController
@CrossOrigin("*")
public class FeriadoController {
@Autowired
private FeriadoDAO dao;




    @PostMapping("/feriado/cadastro")
    public ResponseEntity<Feriado> cadastroFeriado(@RequestBody Feriado feriado) {
        Feriado novoFeriado = dao.save(feriado);
        if(novoFeriado != null){
            return ResponseEntity.ok(novoFeriado);
        }
        return ResponseEntity.badRequest().build();

    }

    @GetMapping("/feriados/todos")
    public ResponseEntity<List<Feriado>> buscarTodos(){
        List<Feriado> lista = (List<Feriado>)dao.findAll();
 
        if(lista != null){
            return ResponseEntity.ok(lista);
        }else{
            return ResponseEntity.status(404).build();
        }

    }

    /*@GetMapping("/feriados/agencia/{numAgencia}")
    public List<Feriado> findByAgencia(@PathVariable Integer numAgencia){
        List<Feriado> lista = dao.findByAgencia(numAgencia);
        return lista;
    }*/

    /*@GetMapping("/feriados/agencias/{numAgencia}")
    public List<Feriado> buscarPorAgencia(@PathVariable Integer numAgencia){
        List<Feriado> lista = dao.buscarPorAgencia(numAgencia);
        return lista;
    }*/

    @GetMapping("/feriados/agencia/{numAgencia}")
    public List<Feriado> getFeriadoPorAgencia(@PathVariable int numAgencia){
        List<Feriado> lista = dao.buscarPorAgencia(numAgencia);
        return lista;
        
    }
    /*
    @PostMapping("/feriados/agencias/")
    public List<Feriado> getFeriadoAgencia(@RequestBody int agencia){
        List<Feriado> lista = dao.buscarPorAgencia(agencia);
        return lista;
        
    }*/


    
}
