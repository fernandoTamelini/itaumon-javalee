package br.javalee.itaumon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.javalee.itaumon.dao.UsuarioDAO;
import br.javalee.itaumon.model.Usuario;

@RestController
@CrossOrigin("*")
public class UsuarioController {
    @Autowired
    private UsuarioDAO dao;

    @PostMapping("/usuario/login")
    public ResponseEntity<Usuario> fazerLogin(@RequestBody Usuario user) {
        Usuario userEncontrado = dao.findByRacfOrFuncional(user.getRacf(), user.getFuncional());

        if (userEncontrado != null) {
            
            if (userEncontrado.getSenha().equals(user.getSenha())) {
                userEncontrado.setSenha("*****");
                return ResponseEntity.ok(userEncontrado);
            } else {return ResponseEntity.status(403).build();}
            
        }
        return ResponseEntity.status(403).build();
        
    }

    

}
